// Copyright (c) 2021 Greg Griffith
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "test_threadsupport.h"

int main()
{
    psm_promotion_tests();
    psm_simple_tests();
    psm_starvation_tests();
    rsm_promotion_tests();
    rsm_simple_tests();
    rsm_starvation_tests();
    printf("all tests passed\n");
}
