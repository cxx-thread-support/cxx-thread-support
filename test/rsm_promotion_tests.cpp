// Copyright (c) 2019 Greg Griffith
// Copyright (c) 2019 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "recursive_shared_mutex.h"
#include "test_threadsupport.h"

#ifdef DEBUG_LOCKORDER

class rsm_watcher : public recursive_shared_mutex
{
public:
    size_t get_shared_owners_count() { return _read_owner_ids.size(); }
    std::thread::id get_promotion_candidate() { return _promotion_candidate_id.load(); }
};

static rsm_watcher rsm;
static std::vector<int> rsm_guarded_vector;
static std::atomic<int> shared_locks {0};

void helper_fail()
{
    // should not be able to lock
    assert(rsm.try_lock() == false);
}

void helper_pass()
{
    assert(rsm.try_lock() == true);
    // unlock the try_lock
    rsm.unlock();
}

// test locking shared while holding exclusive ownership
// we should require an equal number of unlock_shared for each lock_shared
void rsm_lock_shared_while_exclusive_owner()
{
    // lock exclusive 3 times
    rsm.lock();
    rsm.lock();
    rsm.lock();

    // lock_shared twice
    rsm.lock_shared();
    rsm.lock_shared();

    // it should require 3 unlocks and 2 unlock_shareds to have another thread lock exclusive

    // dont unlock exclusive enough times
    rsm.unlock();
    rsm.unlock();
    rsm.unlock_shared();
    rsm.unlock_shared();

    // we expect helper_fail to fail
    std::thread one(helper_fail);
    one.join();

    // relock
    rsm.lock();
    rsm.lock();
    rsm.lock_shared();
    rsm.lock_shared();

    // now try not unlocking shared enough times
    rsm.unlock();
    rsm.unlock();
    rsm.unlock();
    rsm.unlock_shared();

    // again we expect helper fail to fail
    std::thread two(helper_fail);
    two.join();

    // unlock the last shared
    rsm.unlock_shared();

    // helper pass should pass now
    std::thread three(helper_pass);
    three.join();
}

static void shared_only()
{
    rsm.lock_shared();
    shared_locks.store(shared_locks.load() + 1);
    while (rsm.get_promotion_candidate() == NON_THREAD_ID) ;
    rsm.unlock_shared();
}

static void exclusive_only()
{
    rsm.lock();
    rsm_guarded_vector.push_back(4);
    rsm.unlock();
}

static void promoting_thread()
{
    while (shared_locks.load() != 1) ;
    rsm.lock_shared();
    shared_locks.store(shared_locks.load() + 1);
    bool promoted = rsm.try_promotion();
    assert(promoted == true);
    rsm_guarded_vector.push_back(7);
    rsm.unlock();
    rsm.unlock_shared();
}

/*
 * if a thread askes for a promotion while no other thread
 * is currently asking for a promotion it will be put in line to grab the next
 * exclusive lock even if another threads are waiting using lock()
 *
 * This test covers lock promotion from shared to exclusive.
 *
 */
void rsm_try_promotion()
{
    // clear the data vector at test start
    rsm_guarded_vector.clear();
    // test promotions
    std::thread one(shared_only);
    std::thread two(promoting_thread);
    while (shared_locks.load() != 2) ;
    std::thread three(exclusive_only);

    one.join();
    two.join();
    three.join();

    // 7 was added by the promoted thread, it should appear first in the vector
    rsm.lock_shared();
    assert(7 == rsm_guarded_vector[0]);
    assert(4 == rsm_guarded_vector[1]);
    rsm.unlock_shared();
}

void rsm_promotion_tests()
{
    rsm_lock_shared_while_exclusive_owner();
    rsm_try_promotion();
}

#else // ifndef DEBUG_LOCKORDER

void rsm_promotion_tests()
{
    printf("Compile with Debug Lockorder to enable the rsm promotion tests\n");
}

#endif // end ifdef DEBUG_LOCKORDER
